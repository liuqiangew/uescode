// 工具组合包
layui.define('jquery',function(exports){
	"use strict";
	var $ = layui.jquery,
	tool = {
		map:function(ele) { // 可用于获取多选框的值，并返回数组
			var data = $(ele).map(function(){return this.value;}).get();
			return data;
		},
		in_array:function(needle,haystack,splits) { // 一个变量是否在一个数组中
			//needle 待检测字符串,haystack 数组或者是以splits分割的字符串
			var splits = typeof splits=='undefined'?',':splits;
			var haystack=this.is_array(haystack)?haystack:haystack.split(splits);
			if(typeof needle == 'string' || typeof needle == 'number') {
				for(var i in haystack) {
					if(haystack[i] == needle) {
						return true;
					}
				}
			}
			return false;
		},
		is_array:function(arr) { // 是否是一个数组
			return toString.apply(arr) === '[object Array]';
		},
		ajax:function(params,dataType) { // jquery Ajax封装
			var result = null,err = null,options = {
				url:'',
				type:'GET', //请求方式
				dataType:'html', // 返回数据类型
				async:false, // 默认同步调用数据，可返回result
				data:'' // POST数据必须设置
			};
			if (typeof(params)=='string') { // GET获取html，如果设置dataType,GET获取dataType类型数据
				params = {url:params};
				params.dataType = dataType || 'html'; // 第一个参数必须为字符串时此参数才生效
			}
			$.extend(options, params || {});
			if ($.trim(options.url)=='') {
				console.log('url参数必须设置。');
				return undefined;
			}
			options.type = options.type.toUpperCase(); // 可选参数：GET,POST
			options.dataType = options.dataType.toLowerCase(); // 可选参数：xml,html,text,json,script,jsonp
			if (options.dataType=='script' || options.dataType=='jsonp') {
				// 如果指定了script或者jsonp类型，不返回XMLHttpRequest对象，因此需要使用回调函数处理数据
				if (typeof(options.callback)=="undefined" || typeof(options.callback)!="function") {
					console.log('dataType为script或jsonp时，必须设置回调函数。');
					return undefined;
				} else {
					// 重写 success回调函数
					options.success = function(html) {options.callback(html);}
				}
			}
			if (typeof(options.callback)=="function") { // 使用回调函数
				// 重写 success回调函数
				options.success = function(html) {
					options.callback(html);
					result = html;
				}
			}
			var settings = {
				timeout: 6000,
				success: function(html) {result = html;},
				error: function() {err = 1;}
			};
			$.extend(settings, options || {});
			$.ajax(settings);
			if (err==1) {
				console.log('数据读取失败，请重试！');
				return undefined;
			} else {
				return result;
			}
		},
		escE:function(callback,ele,one) {
			var that = this;
			ele = ele || document;
			function _ea(e){
				if(e && e.keyCode===27){
					callback.call(that);
					if (typeof(one)!='undefined') {$(ele).off('keydown', _ea);return false;}
				}
			};
			$(ele).on('keydown', _ea);
		},
		enterE:function(callback,ele,one) {
			var that = this;
			ele = ele || document;
			function _ea(e){
				if(e && e.keyCode===13 && !e.ctrlKey){
					callback.call(that);
					if (typeof(one)!='undefined') {$(ele).off('keydown', _ea);return false;}
				}
			};
			$(ele).on('keydown', _ea);
		},
		ctrlEnterE:function(callback,ele,one) {
			var that = this;
			ele = ele || document;
			function _ea(e){
				if(e && e.ctrlKey && e.keyCode==13){
					callback.call(that);
					if (typeof(one)!='undefined') {$(ele).off('keydown', _ea);return false;}
				}
			};
			$(ele).on('keydown', _ea);
		}
	};
	exports('tool', tool);
});