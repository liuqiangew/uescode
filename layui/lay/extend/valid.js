// 验证组合包
layui.define('layer',function(exports){
	"use strict";
	var $ = layui.jquery,
	valid = {
		form:function(formElem,settings) {
			// 表单验证:formElem为class或id里的值，如'.regForm';在需要验证的表单项目添加title属性,则替代默认错误提示
			var that = this;
			var verify = {
				required: [
				  /[\S]+/,
				  '必填项不能为空'
				],
				phone: function(value,item,tips){
					var ele = that.mobile(value);
					if (ele!=true) return tips || ele;
				},
				tel: [
				  /^[+]{0,1}(\d){1,4}[ ]{0,1}([-]{0,1}((\d)|[ ]){1,12})+$/,
				  '请输入正确的固定电话号码'
				],
				email: [
				  /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/,
				  '邮箱格式不正确'
				],
				url: [
				  /(^#)|(^http(s*):\/\/[^\s]+\.[^\s]+)/,
				  '链接格式不正确'
				],
				digits: [
				  /^\d+$/,
				  '只能填写数字'
				],
				number:function(value,item,tips) {
					var ele = that.number(value);
					if (ele!=true) return tips || ele;
				},
				date: [
				  /^(\d{4})[-\/](\d{1}|0\d{1}|1[0-2])([-\/](\d{1}|0\d{1}|[1-2][0-9]|3[0-1]))*$/,
				  '日期格式不正确'
				],
				identity: [
				  /(^\d{15}$)|(^\d{17}(x|X|\d)$)/,
				  '请输入正确的身份证号'
				],
				gtzero: function(value,item,tips){
					var value_preg = /^\d+$/;
					if (value<=0 || !value_preg.test(value)) return tips || ele;
				}
			};
			if (typeof(settings) != 'undefined') $.extend(true, verify, settings);
			var DANGER = 'layui-form-danger',stop = null,elem = $(formElem),verifyElem = elem.find('*[valid]'); //获取需要校验的元素
			//开始校验
			layui.each(verifyElem, function(_, item){
			  var othis = $(this), ver = othis.attr('valid'), tips = othis.attr('title') || othis.attr('tips');
			  var value = $.trim(othis.val()), isFn = typeof verify[ver] === 'function';
			  othis.removeClass(DANGER);
			  var Fun = (isFn ? tips = verify[ver](value, item, tips) : !verify[ver][0].test(value));
			  if (typeof(othis.attr('empty'))!='undefined') {
				  if (value=='') Fun = false; 
			  }
			  if(verify[ver] && Fun) {
				//非移动设备自动定位焦点
				var device = layui.device();
				if (!device.android && !device.ios) {
					layui.layer.msg(tips || verify[ver][1],{icon:7,time:2000,anim:6});
					item.focus();
				} else {
					layui.layer.msg(tips || verify[ver][1],{time:2000});
				}
				othis.addClass(DANGER);
				return stop = true;
			  }
			});
			if(stop) return false;
			return true;
		},
		email:function(value) { // 邮箱验证
			var email_preg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (value=='' || !email_preg.test(value)) {
				return '邮箱格式不正确';
			} else {
				return true;
			}
		},
		mobile:function(value) { // 验证手机
			var mobile_preg = /^0?(13|14|15|16|17|18|19)[0-9]{9}$/;
			if (value=='' || !mobile_preg.test(value)) {
				return '手机号码格式不正确';
			} else {
				return true;
			}
		},
		tel:function(value) { // 验证电话号码
			if (!/^[+]{0,1}(\d){1,4}[ ]{0,1}([-]{0,1}((\d)|[ ]){1,12})+$/.test(value)) {
				return '手机或电话号码格式不正确';
			} else {
				return true;
			}
		},
		date:function(value) {
			if (/Invalid|NaN/.test(new Date(value))) {
				return '日期格式不正确';
			} else {
				return true;
			}
		},
		password:function(value) { // 验证密码合法性
			if (value === "") {return "密码不能为空";}
			var regExpDigital = /\d/; //如果有数字
			var regExpLetters = /[a-zA-Z]/; //如果有字母
			if (!(regExpDigital.test(value) && regExpLetters.test(value) && value.length >= 6)) {return '密码必须包含英文和数字且6位以上';}
			return true;
		},
		number:function(value) {
			if (!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value)) {
				return '不是有效的数字';
			} else {
				return true;
			}
		},
		url:function(value) {
			if (!/(^#)|(^(https?|ftp):\/\/[^\s]+\.[^\s]+)/.test(value)) {
				return '不是有效的网址';
			} else {
				return true;
			}
		},
		english:function(value){
			if (!/^[a-z_A-Z]+$/.test(value)) {
				return '必须是英文字母';
			} else {
				return true;
			}
		},
		en_num:function(value){
			if (!/^\w+$/.test(value)) {
				return '必须是英文数字或下划线';
			} else {
				return true;
			}
		},
		chinese:function(value){
			if (!/^[\u4e00-\u9fa5]+$/.test(value)) {
				return '必须是汉字';
			} else {
				return true;
			}
		},
		cn_username:function(value){
			if (!/^([\u4e00-\u9fa5]|[\w])+$/.test(value)) {
				return '必须是中文英文下划线或数字';
			} else {
				return true;
			}
		},
		digits:function(value){
			if (!/^\d+$/.test(value)) {
				return '必须是数字';
			} else {
				return true;
			}
		}
	};
	exports('valid', valid);
});