// layerPC端扩展
layui.define('layer',function(exports){
	"use strict";
	var layer = layui.layer,$ = layui.jquery,
	dialog = {
		open: function(url,title,width,height,dialog_full) {
			var ismaxmin,onend,onsuccess,oncancel;
			var obj = url;
			// 以{url:'',title:'',width:0,height:0,end:func,success:func,cancel:func,maxmin:true} 方式设置数据，第二个参数不为空，覆盖{title:''}
			if (typeof(obj)==='object') {
				url = obj.url;
				title = title || obj.title;
				width = obj.width;
				height = obj.height;
				onend = obj.end;
				onsuccess = obj.success;
				oncancel = obj.cancel;
				dialog_full = obj.maxmin==true?1:0;
			}
			url = url || 'http://www.baidu.com/';
			title = title || '信息提示框';
			if (height == undefined || height==0){height=($(window).height()-60)+'px';ismaxmin=true;}else{height=height+'px';ismaxmin=false;}
			if (width  == undefined || width==0){width=($(window).width()-60)+'px';ismaxmin=true;}else{  width=width+'px';ismaxmin=false;}
			if (dialog_full==1) ismaxmin=false;
			var p = { type: 2,title: title,area: [width, height],maxmin: ismaxmin,content: url,end:function(){if (typeof(onend)==="function") {onend();}},success:function(layero,index){if (typeof(onsuccess)==="function") {onsuccess(layero,index);}},cancel:function(index,layero){if (typeof(oncancel)==="function") {oncancel(layero,index);}} };
			var lay = layer.open(p);
			if (dialog_full==1) layer.full(lay);
		},
		alert: function(msg,url,success) {
			var type = 1;
			if (success==undefined) type = 7;
			layer.alert(msg,{title:'提示',closeBtn:0,icon:type,yes:function(index){
				if (typeof(url) === "function") {
					url.call(this);
				} else {
					url = url || '';
					if (url != '') location.href = url;
				}
				layer.close(index);
			}});
		},
		alert2: function(msg,p,success) { // p:三种方式：第一种不填参数，当前页重载；第二种回调函数；第三种任意值父窗口重载
			var ele = this;
			var type = 7;
			if (success==undefined) type = 1;
			layer.alert(msg,{title:'提示',closeBtn:0,icon:type,yes:function(index){
				if (p==undefined) {
					ele.reload();
				} else if (typeof(p) === "function") {
					p.call(this);
					layer.close(index);
				} else {
					parent.location.reload();
				}
			}});
		},
		confirm: function(msg,callback) { // confirm框，可使用回调函数
			layer.confirm(msg,{title:'询问',icon:0},function(index){
				if (typeof(callback) === "function") callback.call(this);
				layer.close(index);
			});
		},
		msg: function(content,callback,success) {
			if (typeof(callback) !== "function") callback = null;
			if (success==undefined) {
				layer.msg(content,{icon:7,time:2000,anim:6},callback);
			} else {
				layer.msg(content,{icon:1,time:1500},callback);
			}
		},
		tips: function(title,callback) {
			if (typeof(callback) !== "function") callback = null;
			layer.msg(title,{time:2000},callback);
		},
		layerTips: function(msg,obj,pos,color) {
			// layer提示信息 obj:在哪个位置提示（可以是#id或.class） pos位置:1上2右3下4左 color:提示背景颜色
			pos = pos || 2;
			color = color || '#F55829';
			layer.tips(msg,obj,{tips: [pos, color]});
		},
		loading: function(icon) {// 加载框，不会自动关闭，需要配合close(index)使用
			icon = icon || 0;
			return layer.load(icon,{shade:[0.1,'#fff']}); // 返回index
		},
		close: function(index) {
			if (typeof(index)==="undefined") {
				// 没填参数，关闭最新打开的一个窗口
				var _index = $('.layui-layer').last().attr('times');
				layer.close(_index);
			} else if (typeof(index)==="number") {
				layer.close(index);
			} else {
				layer.closeAll();
			}
		},
		reload: function(p){// 重载当前窗口
			if (p==undefined) {
				location.reload();
			} else {
				parent.location.reload();
			}
		},
		refresh: function(p){// 刷新当前窗口
			if (p==undefined) {
				location.href = location.href;
			} else {
				parent.location.href = parent.location.href;
			}
		},
		resize: function(){// 重置iframe窗口大小，不要使用parent调用resize
			var index = parent.layui.layer.getFrameIndex(window.name); //获取窗口索引
			parent.layui.layer.iframeAuto(index);
			var layero = parent.$('#layui-layer' + index);
			var offsetTop = ($(parent.window).height() - layero.outerHeight())/2;
			layero.css({top:offsetTop});
		}
	};
	exports('dialog', dialog);
});