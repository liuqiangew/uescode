// layer移动端扩展
layui.define('mlayer',function(exports){
	"use strict";
	var layer = layui.mlayer,$ = layui.jquery,skinName = 'liuAlertSkin',
	mdialog = {
		// 打开一个全屏的页面:content内容，callback回调函数，className自定义样式
		open: function(content,callback,className) {
			className = className || '';
			layer.open({
				type: 1,
				content: '<div class="layerDialogStyle"><div class="layui-liu-page"><div class="layui-liu-content">'+content+'</div></div></div>',
				anim: 'up',
				style: 'position:fixed;overflow:auto;left:0;top:0;width:100%;height:100%;border:none;-webkit-animation-duration:.5s;animation-duration:.5s;',
				className:className,
				success:function(){
					document.getElementsByTagName("html")[0].style.setProperty('overflow','hidden');
					if (typeof(callback) === "function") {
						callback.call(this);
					}
				},
				end:function(){
					document.getElementsByTagName("html")[0].style.removeProperty('overflow');
				}
			});
		},
		alert: function(msg,url) {
			url = url || '';
			if (url=='') {
				layer.open({
					className:skinName,
					content: this._parseMsg(msg),
					btn: ['确定']
				});
			} else {
				layer.open({
					className:skinName,
					content: this._parseMsg(msg),
					btn: ['确认', '取消'],
					shadeClose: false,
					yes: function(index){
						if (typeof(url) === "function") {
							url.call(this);
							layer.close(index);
						} else {
							location.href = url;
						}
					}, no: function(){
						
					}
				});
			}
		},
		alert2: function(msg,url) {
			url = url || '';
			if (url=='') {
				layer.open({
					className:skinName,
					content: this._parseMsg(msg),
					btn: ['确定']
				});
			} else {
				layer.open({
					className:skinName,
					content: this._parseMsg(msg),
					btn: ['确定'],
					shadeClose: false,
					yes: function(index){
						if (typeof(url) === "function") {
							url.call(this);
							layer.close(index);
						} else {
							location.href = url;
						}
					}
				});
			}
		},
		confirm: function(msg,callback) {
			layer.open({className:skinName,content:this._parseMsg(msg),btn:['确认','取消'],shadeClose:false,yes:function(index){
					if (typeof(callback) === "function") callback.call(this);
					layer.close(index);
				}
			});
		},
		msg: function(content,callback) {
			this.tips(content,callback);
		},
		tips: function(msg,callback) {
			var times = 2; // 几秒后关闭
			if (typeof(callback) !== "function" && !isNaN(callback)) times = callback;
			layer.open({content:this._parseMsg(msg),skin:'msg',time:times,end:function(){
				if (typeof(callback) === "function") callback.call(this);
			}});
		},
		loading: function(msg) {// 加载框，不会自动关闭，需要配合close(index)使用
			msg = msg || '';
			return layer.open({type:2,shadeClose:false,shade:'background-color: rgba(0,0,0,.2)',content:msg,className:'mdialog-loading'}); // 返回index
		},
		close: function(index) {
			if (typeof(index)==="undefined") {
				// 没填参数，关闭最新打开的一个窗口
				var _index = $('.layui-m-layer').last().attr('index');
				layer.close(_index);
			} else if (typeof(index)==="number") {
				layer.close(index);
			} else {
				layer.closeAll();
			}
		},
		_parseMsg: function(msg) { // 解析字号
			return '<div class="mdialog-content" style="font-size:16px;line-height:140%;">'+msg+'</div>';
		},
		reload: function(){
			location.reload();
			return false;
		},
		refresh: function(){
			location.href = location.href;
			return false;
		}
	};
	exports('mdialog', mdialog);
});