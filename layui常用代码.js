/*
<script>
var UES={
	WEB_DIR:"{$web_dir}",VIEW_PATH:"./",WEB_VIEW:'{$web_view}',HOST:window.location.protocol+'//'+window.location.hostname
	,REF:'{$current_url_code}'
	{if $is_logined==1},UID: {$userData.id}{/if}
};
</script>
*/
// 简单调用
layui.use(['tool','dialog','valid'],function(){
	var tool = layui.tool, dia = layui.dialog, valid = layui.valid;
});


layui.use(['layer','tool','dialog','valid','element','upload','form','upload','laydate'],function(){
	var tool = layui.tool, layer = layui.layer, dia = layui.dialog, valid = layui.valid, element = layui.element;
	var upload = layui.upload,laydate = layui.laydate;
	
	// 获取某个表单里的内容
	$('.btns',$('表单name或class')).html();
	
	// ajax验证
	var mobile = $.trim($("input[name='mobile']").val());
	var postData = {mobile:mobile};
	result = tool.ajax({url:UES.WEB_DIR+'?a=ajax&m=base&ac=existsMobile',dataType:'json',type:'post',data:postData});
	var layno = dia.loading();
	if (result.error==0) {
		dia.close(layno);
		dia.msg(result.data, null,1);
		//dia.msg(result.data, function(){dia.reload();},1);
	} else {
		dia.close(layno);
		dia.msg(result.msg);
		return false;
	}
	
	// 另一种
	if (result.error==-1) {
		dia.close(layno);
		dia.msg(result.msg);
		return false;
	}
	
	
	// 异步更新
	dia.loading();
	postData = {act:'getShowDialog'};
	tool.ajax({url:UES.CUR_PATH,dataType:'json',type:'post',data:postData,async:true,callback:function(result){
		dia.close();
		dia.msg(result.data, null,1);
	}});
	
	// 多按钮操作 type:1设置此参数时，弹窗里再提示不会关闭。默认是消息弹窗，不能存在多个
	layer.open({
		content: '您需要登录或注册后才能去结算，是否登录？'
		,btn: ['现在登录', '取消']
		,yes: function(index, layero){
			location.href='{$login_url}';
		}
		,btn2: function(index, layero){
		//按钮【按钮二】的回调
		
		//return false 开启该代码可禁止点击该按钮关闭
		}
		,icon:3 // 0感叹号，1成功，2失败，3提示
		,closeBtn:0
	});

	mlayer.open({
		content: '您需要登录或注册后才能去结算，是否登录？'
		,btn: ['现在登录', '取消']
		,yes: function(index){
			location.href='{$login_url}';
		}
		,no: function(){
			alert('取消');
		}
	});
	
	/*
	<a href="javascript:;" class="showTips"><span class="ues-icon">&#xe680;</span><p class="tipsDiv hidden">第5课时</p></a>
	
	css:
	.showTips,.noLink {text-decoration: none!important; cursor: pointer;}
	.liuTips .layui-layer-content {color: #000!important;}
	*/
	$('.showTips').on('mouseenter',function(){
		this.index = layer.tips($(this).find('.tipsDiv').text(), $(this), {
		  tips: [3, '#FFF'],skin:'liuTips',time:-1
		});
	}).on('mouseleave',function(){
		layer.close(this.index);
	});
	
	//Tips  <div lay-tips="我是提示内容"></div>
	$('*[lay-tips]').on('mouseenter', function(){
		var content = $(this).attr('lay-tips');
		this.index = layer.tips('<div style="color: #666;">'+ content + '</div>', this, {
			tips: [3, '#FFF'],skin:'liuTips',time:-1
		});
	}).on('mouseleave', function(){
		layer.close(this.index);
	});
	
	// 手机端Ajax打开新窗口
	$(document).on('click','.openDialog',function(e){
		e.preventDefault();
		content = tool.ajax($(this).attr('href'));
		dia.open(content,function(){
			// 关闭
			$(document).on('click','.closeDialog',function(){
				dia.close();
			});
		});
	});
	
	// 随机数数字
	var randStr = Math.random().toString().substr(2,7);
	// 动态laydate使用，注意lay-key=""属性，要不同（可写入随机数），每次新增动态数据后，执行renderDate()
	function renderDate() {
		lay('.dateSelect').each(function(){
			laydate.render({
				elem: this
				,trigger: 'click'
			});
		});
	}
	renderDate();
});