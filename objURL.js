/* 
说明：本代码可自由复制修改并且使用，但请保留作者信息！
Author: Kevin  WebSite: http://iulog.com/  QQ:251378427

JS 操作 URL 函数使用说明：
初始化 var myurl=new objURL(); //也可以自定义URL： var myurl=new objURL('http://iulog.com/?sort=js'); 
读取url参数值 var val=myurl.get('abc'); // 读取参数abc的值
设置url参数 myurl.set("arg",data); // 新增/修改 一个arg参数的值为data
移除url参数 myurl.remove("arg","arg2",……); //移除arg参数
获取处理后的URL myurl.url();//一般就直接执行转跳 location.href=myurl.url();
调试接口：myurl.debug(); //修改该函数进行调试
提示：PHP调用时，如果参数存在中文，获取前需使用urldecode()
 */
function objURL(url){
	var ourl=url||window.location.href;
	var href="";//?前面部分
	var params={};//url参数对象
	var jing="";//#及后面部分
	var init=function(){
		var str=ourl;
		var index=str.indexOf("#");
		if(index>0){
			jing=str.substr(index);
			str=str.substring(0,index);
		}
		index=str.indexOf("?");
		if(index>0){
			href=str.substring(0,index);
			str=str.substr(index+1);
			var parts=str.split("&");
			for(var i=0;i<parts.length;i++){
				var kv=parts[i].split("=");
				params[kv[0]]=kv[1];
			}
		}else{
			href=ourl;
			params={};
		}
	};
	this.set=function(key,val,encode){
		if (typeof(encode)=='undefined') {
			params[key]=encodeURI(val);
		} else {
			params[key]=encodeURIComponent(val);
		}
	};
	this.remove=function(){
		for(i=0;i<arguments.length;i++ ){  
			key = arguments[i];
			if(key in params) params[key]=undefined;
		}
	};
	this.get=function(key){
		return params[key];
	};
	this.url=function(key){
		var strurl=href;
        var objps=[];
        for(var k in params){
            if(params[k]){
                objps.push(k+"="+params[k]);
            }
        }
        if(objps.length>0){
            strurl+="?"+objps.join("&");
        }
        if(jing.length>0){
            strurl+=jing;
        }
        return strurl;
	};
	this.debug=function(){
		// 以下调试代码自由设置
		var objps=[];
		for(var k in params){
			objps.push(k+"="+params[k]);
		}
		alert(objps);//输出params的所有值
	};
	init();
}



/*
=============排序PHP：
$sort_by = strtolower(ues::nohtml(trim(ues::g('sort_by','id'))));
$sort_order = strtolower(ues::nohtml(trim(ues::g('sort_order','desc'))));
$sort_order = $sort_order=='desc'?'desc':'asc';
// 字段不存在调用默认字段
if (mo::field_exists($sort_by,$table,0)!=true) $sort_by = 'id';
$tpl->assign('sort_by',$sort_by);
$tpl->assign('sort_order',$sort_order);
$orderby = " ORDER BY `$sort_by` $sort_order,id DESC"; // 排序
=============

// 使用案例 fym.liu.com
var ajaxData = new objURL();
$(function(){
	$(document).on('click','.ajax_item_id',function(){
		ele_name = $(this).attr('ele_name');
		ele_id = $(this).attr('ele_id');
		ajaxData.set(ele_name,ele_id);
		getData();
	});
});
// 排序
//使用：<li class="{if $sort_by=='id'}active{/if} {$sort_order}"><a href="javascript:;" onClick="sortList('id','{if $sort_by=='id'}{$sort_order}{else}asc{/if}');">默认 {if $sort_by=='id'}{if $sort_order=='asc'}↑{else}↓{/if}{/if}</a></li>
function sortList(sort_by,sort_order) {
	sort_order = sort_order.toLowerCase()=='asc'?'desc':'asc';
	ajaxData.set('page',1);
	ajaxData.set('sort_by',sort_by);
	ajaxData.set('sort_order',sort_order);
	getData();
}


function getPage(page) {
	ajaxData.set('page',page);
	getData(1);
}
function getData(first_page) {
	ajaxData.set('ajaxlist',1);
	if (typeof first_page == 'undefined') ajaxData.set('page',1);
	var url = ajaxData.url();
	
	_url = url.replace('&ajaxlist=1','');
	_url = _url.replace('&page=1','');
	if (getIEVersion()>9) {
		result = layui.tool.ajax(url);
		$('.content').html(result);
		window.history.pushState("", '标题', _url);
	} else {
		location.href=_url;
	}
}
function getIEVersion() {
	var userAgent = navigator.userAgent;
	var isOpera = userAgent.indexOf("Opera") > -1;
	var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera;
	if (isIE) {
		var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
		reIE.test(userAgent);
		var fIEVersion = parseFloat(RegExp["$1"]);
		return fIEVersion;
	} else {
		return 10;
	}
}
*/